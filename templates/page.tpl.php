<header>
	<!--div class='header-top'> 
		<div class="ink-grid">
			 <?php if ($logo): ?>
	    		<a class="brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
	    		  <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
	    		</a>
	  	  <?php endif; ?>
	  	  <?php if ($site_name): ?>
	    		<h1>
	    			<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="brand"><?php print $site_name; ?></a>
	    		</h1>
	    	<?php endif; ?>
				<?php if ( $site_slogan ): ?>
	    		<?php print $site_slogan; ?>
	   		<?php endif; ?>
		</div>	
	</div-->
	
	<div class='header-top'>
		<div class='ink-grid'>
			<nav class="ink-navigation"> 
				<?php //if ($primary_nav): ?>
		    	<?php //print $primary_nav; ?>
		    <?php //endif; ?>
		   	<ul class="menu horizontal flat shadowed black sticky">
		      <li><a href="#">Item</a></li>
		      <li><a href="#">Item</a></li>
		   	</ul>
			</nav> 
		</div>
	</div>
</header>

<div class="ink-grid">
		<div> <?php print $messages; ?></div>
  	<div  class='column-group gutters'> 
    	<?php if ($page['sidebar_first']): ?>
      	<div class="large-25">
      		<div class='box'>
      			<?php print render($page['sidebar_first']); ?>
      		</div>
        	
      	</div>  <!-- /#sidebar-first -->
    	<?php endif; ?>  
    	
    	<div class="large-75">
	    	<?php if ($page['highlighted']): ?>
	        <div class="highlighted hero-unit"><?php print render($page['highlighted']); ?></div>
	      <?php endif; ?>
	      
	      <?php if ($breadcrumb): print $breadcrumb; endif;?>
	      <?php if ($title): ?>
	        <h2 class="page-title"><?php print $title; ?></h2>
	      <?php endif; ?>
	      
	      
	      <?php if ($tabs): ?>
	        <?php print render($tabs); ?>
	      <?php endif; ?>
	      
	      <?php if ($page['help']): ?> 
	        <div class="well"><?php print render($page['help']); ?></div>
	      <?php endif; ?>
	      
	      <?php if ($action_links): ?>
	        <ul class="action-links"><?php print render($action_links); ?></ul>
	      <?php endif; ?>
    		
    		<?php print render($page['content']); ?>
    	</div>
	    
	    <?php if ($page['sidebar_second']): ?>
	      <div class="large-20" id='sidebar-second">
	        <?php print render($page['sidebar_second']); ?>
	      </div>  <!-- /#sidebar-second -->
	    <?php endif; ?>

  </div>
</div>

<footer class="footer container">
	<div class='ink-grid'> 
		<div class="column-group gutters">
			<div class='large-40'>
				<?php print render($page['footer']); ?>
			</div>
		</div>
	</div>
</footer>