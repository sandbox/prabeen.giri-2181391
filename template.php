<?php  

/**
 * Overriding Drupal Breadcrum theme
 * @param unknown $variables
 * @return string
 */
function ink_breadcrumb($variables) {
	$breadcrumb = $variables['breadcrumb'];
	if (!empty($breadcrumb)) {
		$breadcrumbs = "<div class='ink-navigation'>";
		$breadcrumbs .= '<ul class="breadcrumbs">';

		$count = count($breadcrumb) - 1;
		foreach($breadcrumb as $key => $value) {
			$breadcrumbs .= '<li>'.$value.'</li>';
		}
		$breadcrumbs .= '</ul>';
		$breadcrumbs .= '</div>';
		
		return $breadcrumbs;
	}
}

/**
 * Implements hook_theme()
 */
function ink_theme() { 
	return array(
		'twitter_bootstrap_links' => array(
			'variables' => array('links' => array(), 'attributes' => array(), 'heading' => NULL),
		),
		'twitter_bootstrap_btn_dropdown' => array(
			'variables' => array('links' => array(), 'attributes' => array(), 'type' => NULL),
		),
	);
}
/**
 * Implements hook_form_alter() 
 */
function ink_form_alter(&$form, &$form_state, $form_id) { 
	if ($form_id == 'user_login_block') {
		$form['name']['#size'] = 27;
		$form['pass']['#size'] = 27;
	}
}

/**
 * Override theme_status_messages() 
 * @see theme_status_messages(); 
 * @param  $vars
 * @return HTML string
 */
function ink_status_messages($variables) { 
	$display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  //drupal drupal status with Ink class
  $status_class_mapping = array(
  	'status' => 'success', 
  	'error' => 'error', 
  	'warning' => '',
  );  
  
  foreach (drupal_get_messages($display) as $type => $messages) {
  	$ink_alert_class = $status_class_mapping[$type]; 
  	$alert_display_type = count($messages) > 1 ? "block" : "basic"; 
  	$output .= "<div class=\"ink-alert $ink_alert_class $alert_display_type\">\n";
    $output .= "<button class=\"ink-dismiss\">&times;</button>" ; 
    if (count($messages) > 1) {
    	$output .= "<h4>" .  $status_heading[$type] ."</h4>"; 
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= '<p><b>' . $status_heading[$type] . ':</b><br>' . $messages[0] . "</p>";
    }
    $output .= "</div>\n";
  }
  return $output;
}

/**
 * Overriding theme_form()
 */
function ink_form($variables) { 
	$element = $variables['element'];
	//add ink class to the form tag
	$element['#attributes']['class'][] = 'ink-form'; 
	
	if (isset($element['#action'])) {
		$element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
	}
	element_set_attributes($element, array('method', 'id'));
	if (empty($element['#attributes']['accept-charset'])) {
		$element['#attributes']['accept-charset'] = "UTF-8";
	}
	// Anonymous DIV to satisfy XHTML compliance.
	return '<form' . drupal_attributes($element['#attributes']) . '><div>' . $element['#children'] . '</div></form>';
}

/**
 * Overriding the form button 
 */
function ink_button($variables) { 
	$element = $variables['element'];
	$element['#attributes']['type'] = 'submit';
	element_set_attributes($element, array('id', 'name', 'value'));
	
	$element['#attributes']['class'][] = 'form-' . $element['#button_type'];
	$element['#attributes']['class'][] = 'ink-button';
	
	if (!empty($element['#attributes']['disabled'])) {
		$element['#attributes']['disabled'] = true; 
	}
	
	return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

/**
 * Theme local tasks
 * @see theme_menu_local_tasks()
 */
function ink_menu_local_tasks(&$variables) {
	$output = '';
	if (!empty($variables['primary'])) {
		$variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
		$variables['primary']['#prefix'] .= '<div class="ink-tabs top hide-small">';
		$variables['primary']['#prefix'] .= '<ul class="tabs-nav">';
		$variables['primary']['#suffix'] = '</ul>';
		$variables['primary']['#suffix'] .= '</div>';
		
		$output .= drupal_render($variables['primary']);
	}
	if (!empty($variables['secondary'])) {
		$variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
		$variables['secondary']['#prefix'] .= '<ul class="tabs secondary">';
		$variables['secondary']['#suffix'] = '</ul>';
		$output .= drupal_render($variables['secondary']);
	}

	return $output;
}

/**
 * @see theme_pager() 
 */
function ink_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  //$li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('Previous')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('Previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => t('Next'), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  //$li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last �')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_previous) {
      $items[] = array(
        'class' => array('previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '�',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
        	$page_new = pager_load_array($pager_page_array[$element] + 0, $element, $pager_page_array);
          $items[] = array(
            'class' => array('active'),
            'data' => theme('pager_link', array('text' => $i, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters))
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '�',
        );
      }
    }
    // End generation.
    if ($li_next) {
	    $items[] = array(
	      'class' => array('next'),
	       'data' => $li_next,
	    );
    } 
    
    
    /*if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }*/
    return '<h2 class="element-invisible">' . t('Pages') . '</h2><nav class="ink-navigation">' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pagination','rounded','black')),
    )). "</nav>";
  }
}
